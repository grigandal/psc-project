﻿using System;
using TimeSequenceLib;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime d = new DateTime();
            TimeSequence<int> s = TimeSequence<int>.From(5, d, new TimeSpan(10))
                .Append(18, new TimeSpan(3))
                .Append(4, new TimeSpan(8))
                .Append(10, new TimeSpan(15))
                .Append(18, new TimeSpan(3))
                .Append(4, new TimeSpan(8))
                .Append(10, new TimeSpan(15));
            Console.WriteLine("Got TimeSequence");
            Console.WriteLine(s.gantString());
            Console.WriteLine("Sequence is continouse: " + s.IsContinous());
            Console.WriteLine("Cutted sequence:");
            Console.WriteLine(s.Between(d + new TimeSpan(11), d + new TimeSpan(25)).gantString());
        }
    }
}
