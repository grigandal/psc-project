# psc-project

## TimeSequence

**class TimeSpanElement**

<table class="tg">
<thead>
  <tr>
    <th class="tg-7btt">Method/property</th>
    <th class="tg-7btt">description</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">TimeSpanElement (constructor)</td>
    <td class="tg-0pky">Builds a Picket object from element, start date and duration<br><br>arguments: (T element, DateTime start, TimeSpan span)</td>
  </tr>
  <tr>
    <td class="tg-0pky">element (property)</td>
    <td class="tg-0pky">Readonly property for data associated with picket (Type - T)</td>
  </tr>
  <tr>
    <td class="tg-0pky">start (property)</td>
    <td class="tg-0pky">Readonly property for start point of a picket (Type - DateTime)</td>
  </tr>
  <tr>
    <td class="tg-0pky">end (property)</td>
    <td class="tg-0pky">Readonly property for end point of a picket (Type - DateTime)</td>
  </tr>
  <tr>
    <td class="tg-0lax">span (property)</td>
    <td class="tg-0lax">Readonly property for duration of a picket (Type - TimeSpan)</td>
  </tr>
  <tr>
    <td class="tg-0pky">inSpan (method)</td>
    <td class="tg-0pky">Method to get if the picket is fully lays in datetime interval<br/><br/>arguments: (DateTime first, DateTime second)<br/><br/>(return type: bool)</td>
  </tr>
  <tr>
    <td class="tg-0pky">interceptsSpan (method)</td>
    <td class="tg-0pky">Method to get if the picket is lays in datetime interval, or touches, intercepts or overlaps id<br/><br/>arguments: (DateTime first, DateTime second)<br/><br/>(return type: bool)</td>
  </tr>
</tbody>
</table>

**class TimeSequence**

<table class="tg">
<thead>
  <tr>
    <th class="tg-7btt">Method/property</th>
    <th class="tg-7btt">description</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">TimeSequence (constructor)</td>
    <td class="tg-0pky">Builds an empty time sequence object</td>
  </tr>
  <tr>
    <td class="tg-0pky">Length (method)</td>
    <td class="tg-0pky">Method to get total duration of time sequence in ticks<br/><br/>(return type: int)</td>
  </tr>
  <tr>
    <td class="tg-0pky">IsEmpty (method)</td>
    <td class="tg-0pky">Method to get if the time sequence is empty<br/><br/>(return type: bool)</td>
  </tr>
  <tr>
    <td class="tg-0pky">PicketCount (method)</td>
    <td class="tg-0pky">Method to get count of pickets in the time sequence<br/><br/>(return type: int)</td>
  </tr>
  <tr>
    <td class="tg-0pky">IsContinous (method)</td>
    <td class="tg-0pky">Method to get if the time sequence is continous or has gaps<br/><br/>(return type: bool)</td>
  </tr>
  <tr>
    <td class="tg-0pky">Get (method)</td>
    <td class="tg-0pky">Returns a data associated with picket at current point. If there are no pickets at point and argument "throw_exception" set to "true" - throws ArgumentException, else if "throw_exception" is "false" returns default(T)<br/><br/>arguments: (DateTime point, bool throw_exception = true) <br/><br/>(return type: T)</td>
  </tr>
  <tr>
    <td class="tg-0pky">Find (method)</td>
    <td class="tg-0pky">Returns a picked associated with data if picket exists, else if "throw_exception" is "true" throws ArgumentException else returns default(TimeSpanElement&lt;T&gt;) <br/><br/>arguments: (T value, bool throw_exception = true) <br/><br/>(return type: TimeSpanElement)</td>
  </tr>
  <tr>
    <td class="tg-0pky">Add (method)</td>
    <td class="tg-0pky">Adds picket to sequence and returns result sequence<br/><br/>arguments: (T element, DateTime start, TimeSpan span)<br/><br/>overload arguments: (TimeSpanElement element)<br/><br/>(return type: TimeSequence)</td>
  </tr>
  <tr>
    <td class="tg-0pky">Append (method)</td>
    <td class="tg-0pky">Appends picket to the end of sequence and returns result sequence<br/><br/>arguments: (T element, TimeSpan span, TimeSpan start_delta)<br/><br/>overload arguments: (T element, TimeSpan span)<br/><br/>(return type: TimeSequence)</td>
  </tr>
  <tr>
    <td class="tg-0pky">Between (method)</td>
    <td class="tg-0pky">Returns a part of sequence between two datetime points. If argument "cut" is "true" includes all internal pickets and pickets that touch, intersect or overlap input interval and cuts it by input bounds, else includes only internal pickets<br/><br/>arguments: (DateTime first, DateTime second, bool cut = true)<br/><br/>(return type: TimeSequence)</td>
  </tr>
  <tr>
    <td class="tg-0lax">gantString (method)</td>
    <td class="tg-0lax">Draws (as text) a sequence like a gant diagram<br/><br>arguments: (int width = 120)<br/><br/>(return type: string)</td>
  </tr>
</tbody>
</table>


## Example

```c#
using System;
using TimeSequenceLib;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime d = new DateTime();
            TimeSequence<int> s = TimeSequence<int>.From(5, d, new TimeSpan(10))
                .Append(18, new TimeSpan(3))
                .Append(4, new TimeSpan(8))
                .Append(10, new TimeSpan(15))
                .Append(18, new TimeSpan(3))
                .Append(4, new TimeSpan(8))
                .Append(10, new TimeSpan(15));
            Console.WriteLine("Got TimeSequence");
            Console.WriteLine(s.gantString());
            Console.WriteLine("Sequence is continouse: " + s.IsContinous());
            Console.WriteLine("Cutted sequence:");
            Console.WriteLine(s.Between(d + new TimeSpan(11), d + new TimeSpan(25)).gantString());
        }
    }
}
```

output:
```
Got TimeSequence
|------------------|
5
                   |----|
                   18
                         |--------------|
                         4
                                        |----------------------------|
                                        10
                                                                     |----|
                                                                     18
                                                                           |--------------|
                                                                           4
                                                                                          |----------------------------|
                                                                                          10

Sequence is continouse: True
Cutted sequence:
|----------------|
18
                 |-------------------------------------------------------------------|
                 4
                                                                                     |---------------------------------|
                                                                                     10
```
