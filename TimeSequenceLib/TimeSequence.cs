﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TimeSequenceLib
{
    interface ITimeSpanElement<T>
    {
        T element
        {
            get;
        }

        DateTime start
        {
            get;
        }
        TimeSpan span
        {
            get;
        }

        DateTime end
        {
            get;
        }

        bool inSpan(DateTime first, DateTime second);
    }
    public class TimeSpanElement<T> : ITimeSpanElement<T>, IComparable<TimeSpanElement<T>>
    {
        protected T _element;
        protected DateTime _start;
        protected TimeSpan _span;
        public TimeSpanElement(T element, DateTime start, TimeSpan span)
        {
            this._element = element;
            this._start = start;
            this._span = span;
        }

        public T element => this._element;
        public DateTime start => this._start;
        public DateTime end => this._start + this._span;
        public TimeSpan span => this._span;

        public int CompareTo(TimeSpanElement<T> obj)
        {
            return (int)(this._start - obj.start).Ticks;
        }

        private DateTime max(DateTime first, DateTime second)
        {
            return first < second ? second : first;
        }

        private DateTime min(DateTime first, DateTime second)
        {
            return first > second ? second : first;
        }

        public bool inSpan(DateTime first, DateTime second){
            DateTime f = this.min(first, second), s = this.max(first, second);
            return this.start >= f && this.end <= s;
        }

        public bool interceptsSpan(DateTime first, DateTime second){
            DateTime f = this.min(first, second), s = this.max(first, second);
            return this.inSpan(f, s) || (this.start <= f && this.end >= s) || (this.start >= f && this.start <= s) || (this.end >= f && this.end <= s);
        }
    }
    public class TimeSequence<T> where T : IEquatable<T>
    {
        private List<TimeSpanElement<T>> span_list;
        public TimeSequence()
        {
            this.span_list = new List<TimeSpanElement<T>>();
        }

        public int Length()
        {
            this.span_list.Sort();
            return this.span_list.Count != 0 ? (int)(this.span_list.Last().start - this.span_list[0].start).Ticks + (int)this.span_list.Last().span.Ticks : 0;
        }

        public bool IsEmpty()
        {
            return this.span_list.Count == 0;
        }

        public int PicketCount()
        {
            return this.span_list.Count;
        }

        public bool IsContinous()
        {
            this.span_list.Sort();
            int index = 0;
            foreach (TimeSpanElement<T> e in this.span_list)
            {
                if (index < this.span_list.Count - 1)
                {
                    if (e.start + e.span < this.span_list[index].start)
                    {
                        return false;
                    }
                    index++;
                }
            }
            return true;
        }

        public T Get(DateTime point, bool throw_exception = true)
        {
            foreach (TimeSpanElement<T> e in this.span_list)
            {
                if (e.start <= point && e.start + e.span >= point)
                {
                    return e.element;
                }

            }
            if (throw_exception)
                throw new ArgumentException("No value at datetime = " + point.ToString());
            else return default(T);
        }

        public TimeSpanElement<T> Find(T value, bool throw_exception = true)
        {
            foreach (TimeSpanElement<T> e in this.span_list)
            {
                if (value.Equals(e))
                {
                    return e;
                }

            }
            if (throw_exception)
                throw new ArgumentException("No value = " + value.ToString() + " in time sequence");
            else return default(TimeSpanElement<T>);
        }

        public TimeSequence<T> Append(T element, TimeSpan span, TimeSpan start_delta)
        {
            this.span_list.Sort();
            TimeSpanElement<T> e = new TimeSpanElement<T>(element, this.span_list.Last().start + this.span_list.Last().span + start_delta, span);
            this.span_list.Add(e);
            return this;
        }

        public TimeSequence<T> Add(T element, DateTime start, TimeSpan span)
        {
            this.span_list.Add(new TimeSpanElement<T>(element, start, span));
            this.span_list.Sort();
            return this;
        }

        public TimeSequence<T> Add(TimeSpanElement<T> element)
        {
            this.span_list.Add(element);
            this.span_list.Sort();
            return this;
        }

        public TimeSequence<T> Append(T element, TimeSpan span)
        {
            return this.Append(element, span, TimeSpan.Zero);
        }

        private DateTime max(DateTime first, DateTime second)
        {
            return first < second ? second : first;
        }

        private DateTime min(DateTime first, DateTime second)
        {
            return first > second ? second : first;
        }


        public TimeSequence<T> Between(DateTime first, DateTime second, bool cut = true)
        {
            DateTime f = this.min(first, second), s = this.max(first, second);
            TimeSequence<T> result = new TimeSequence<T>();

            this.span_list.Sort();

            foreach (TimeSpanElement<T> e in this.span_list)
            {
                if (cut)
                {
                    if (e.interceptsSpan(f, s))
                    {
                        DateTime newStart = this.max(e.start, f);
                        DateTime newEnd = this.min(e.end, s);
                        result.Add(e.element, newStart, newEnd - newStart);
                    }
                }
                else
                {
                    if (e.inSpan(f,s))
                    {
                        result.Add(e);
                    }
                }
            }
            return result;

        }

        public string gantString(int width = 120)
        {
            if (this.span_list.Count == 0)
            {
                return "\n";
            }
            this.span_list.Sort();
            TimeSpan totalSpan = this.span_list.Last().end - this.span_list[0].start;
            DateTime start = this.span_list[0].start;
            string result = "";
            foreach (TimeSpanElement<T> e in this.span_list)
            {
                int spaceCount = (int)(e.start - start).Ticks * width / (int)totalSpan.Ticks;
                int stickCount = (int)(e.end - e.start).Ticks * width / (int)totalSpan.Ticks;

                result += new string(' ', spaceCount) + "|" + new string('-', stickCount - 1) + "|\n" + new string(' ', spaceCount) + e.element.ToString() + "\n";
            }
            return result;
        }

        public static TimeSequence<T> From(T element, DateTime start, TimeSpan span) {
            return new TimeSequence<T>().Add(element, start, span);
        }
    }
}
